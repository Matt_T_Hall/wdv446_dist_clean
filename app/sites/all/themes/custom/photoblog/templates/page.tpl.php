<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
 
  <!-- MY Stuff-->
  
  <div id="site-header">
        <div class="container">
            <div id="site-header-image">
                <a href="/">
                    <!--<img src="imgs/logo.png" title="Home" alt="Photography Blog" />-->
					<img src="/PhotoBlog/wdv446_dist_clean/app/<?php print path_to_theme();?>/imgs/logo.png" />
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="menu-nav-container main" id="navigation">
			<?php if ($main_menu || $secondary_menu): ?>			  
				<?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')))); ?>
				<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'clearfix')))); ?>				
			<?php endif; ?>
        </div>
    </div>
    <div class="container main-content">
        <div class="clearfix">
            <div class="three-fourths left-border">
                <!-- Big Image (If Front) -->
				<div class="big-image">
                    <?php if($is_front): ?>
						<?php print render($page['featured_image']); ?>
					<?php endif; ?>	
                </div>
				
				<!-- Title -->
				<?php if ($title): ?>
					<h1 class="title" id="page-title">
						<?php print $title; ?>
					</h1>
				<?php endif; ?>
                <!-- Tabs -->
				<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
				
				<!-- Content -->
				<?php print render($page['content']); ?>
				
            </div>
				<!-- Featured image Block (if front page) -->
            <div class="one-fourth right-column">
                <div class="inner">
                    <?php if($is_front): ?>
						<?php print render($page['sidebar_right']); ?>
					<?php endif; ?>	
                </div>
            </div>
        </div>
    </div>
    <div id="site-footer">
        <div class="container">            
			<?php print render($page['footer']); ?>
			<!--<p>IPhone selvage et aesthetic aliquip. Lomo eiusmod laboris Bushwick Echo Park pug. Nesciunt iPhone gentrify, dreamcatcher officia delectus Tonx Schlitz bicycle rights nihil Odd Future. Pop-up craft beer sunt, Shoreditch tempor meh Brooklyn fixie asymmetrical sartorial mixtape. IPhone mixtape assumenda synth ad church-key, fanny pack sapiente pour-over squid salvia selfies jean shorts Odd Future sint. Pickled meh sustainable, roof party sriracha meggings vero trust fund McSweeney's eiusmod ethnic craft beer tempor butcher. Kitsch Brooklyn Pitchfork before they sold out, non viral cardigan Godard swag anim consequat voluptate.</p>-->
        </div>
    </div>
  
  
  
  
  
  